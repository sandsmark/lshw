#pragma once

#include <QMainWindow>
#include <QProcess>
#include <QPointer>
#include <QDialog>
#include <QHash>
#include <QImage>

class QColumnView;
class QTextBrowser;
class QStandardItemModel;
class QStandardItem;
class QJsonArray;
class QJsonObject;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

private slots:
    void onReload();
    void onSave();
    void onLshwCompleted(int exitCode, QProcess::ExitStatus exitStatus);
    void onOpen();

private:
    void parseOutput(const QByteArray &json);
    void launchLshw();

    QString createDetailHtml(const QJsonObject &object);
    QStandardItem *createItem(const QJsonObject &object, int depth);

    QColumnView *m_columns = nullptr;
    QTextBrowser *m_details = nullptr;
    QStandardItemModel *m_model = nullptr;
    QByteArray m_lastRead;
    QAction *m_saveAction = nullptr;

    QPointer<QProcess> m_lshwProcess;
    QHash<QString, QImage> m_icons;
};
