#include "MainWindow.h"
#include <QColumnView>
#include <QTextBrowser>
#include <QAction>
#include <QDir>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QLocale>
#include <QStandardItemModel>
#include <QToolBar>
#include <QMenuBar>
#include <QStatusBar>
#include <QHBoxLayout>
#include <QTimer>
#include <QImageReader>
#include <QFileDialog>
#include <QSettings>
#include <QApplication>
//#include <QLayout>

MainWindow::MainWindow()
{
//    setLayout(new QVBoxLayout);

    m_model = new QStandardItemModel(this);

    m_details = new QTextBrowser(this);

    m_columns = new QColumnView(this);

    QWidget *dummyWidget = new QWidget(this);
    m_columns->setPreviewWidget(dummyWidget);
    connect(m_columns, &QColumnView::updatePreviewWidget, [dummyWidget] {
        dummyWidget->parentWidget()->parentWidget()->setFixedWidth(0);
    });


    QWidget *widget = new QWidget;
    setCentralWidget(widget);
    QHBoxLayout *mainLayout = new QHBoxLayout;
    widget->setLayout(mainLayout);
    mainLayout->addWidget(m_columns, 2);
    mainLayout->addWidget(m_details, 1);
    m_columns->setResizeGripsVisible(false);

    m_columns->setModel(m_model);
    m_columns->setSelectionMode(QAbstractItemView::SingleSelection);
    m_columns->setSelectionBehavior(QAbstractItemView::SelectItems);
    connect(m_columns->selectionModel(), &QItemSelectionModel::currentChanged, this, [this](const QModelIndex &current, const QModelIndex &) {
            m_details->setHtml(current.data(Qt::UserRole).toString());
    });

    QToolBar *fileToolBar = addToolBar(tr("File"));
    fileToolBar->setMovable(false);
    QAction *reloadAct = fileToolBar->addAction(QIcon::fromTheme("view-refresh"), tr("&Reload"), this, &MainWindow::onReload);
    reloadAct->setShortcut(QKeySequence::Refresh);
    QAction *openAct = fileToolBar->addAction(QIcon::fromTheme("document-open"), tr("&Load data..."), this, &MainWindow::onOpen);
    openAct->setShortcut(QKeySequence::Open);
    m_saveAction = fileToolBar->addAction(QIcon::fromTheme("document-save"), tr("&Save data..."), this, &MainWindow::onSave);
    m_saveAction->setShortcut(QKeySequence::Save);

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(reloadAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(m_saveAction);
    m_saveAction->setEnabled(false);

    QAction *quitAction = fileMenu->addAction(QIcon::fromTheme("application-exit"), tr("&Quit"), qApp, &QApplication::quit);
    quitAction->setShortcut(QKeySequence::Quit);
    quitAction->setMenuRole(QAction::QuitRole);

    QAction *backAction = fileToolBar->addAction(QIcon::fromTheme("go-previous"), tr("&Back"), this, [this]() {
        QModelIndex current = m_columns->selectionModel()->currentIndex();
        if (!current.isValid() || !current.parent().isValid()) {
            return;
        }
        m_columns->selectionModel()->setCurrentIndex(current.parent(), QItemSelectionModel::SelectCurrent);
    });

    connect(m_columns->selectionModel(), &QItemSelectionModel::currentChanged, this, [this, backAction](const QModelIndex &current, const QModelIndex &) {
        m_details->setHtml(current.data(Qt::UserRole).toString());
        backAction->setEnabled(current.isValid() && current.parent().isValid());
    });

    resize(1000, 1000);
    launchLshw();

    const int iconSize = qMax(style()->pixelMetric(QStyle::PM_LargeIconSize), 100);
    for (const QFileInfo &icon : QDir(":/icons/").entryInfoList()) {
        QImageReader reader(icon.absoluteFilePath());
        QSize scaledSize = reader.size();
        scaledSize.scale(iconSize, iconSize, Qt::KeepAspectRatio);
        reader.setScaledSize(scaledSize);

        // qtextdocument nukes the icons for some reason
        m_icons.insert("icon:" + icon.baseName(), reader.read());
    }
    const QStringList extraIcons{
        "input-gamepad",
        "input-keyboard",
        "input-mouse",
        "input-tablet",
        "input-touchpad",
        "speaker",
        "audio-card",
        "audio-input-microphone",
        "camera-web",
    };
    for (const QString &name : extraIcons) {
        QIcon icon = QIcon::fromTheme(name);
        if (icon.isNull()) {
            qWarning() << "Missing icon" << name;
            continue;
        }
        m_icons.insert("icon:" + name, icon.pixmap(iconSize).toImage());

    }
}

void MainWindow::onReload()
{
    launchLshw();
}

void MainWindow::onOpen()
{
    QSettings settings;

    QString filename = QFileDialog::getOpenFileName(this, tr("Select a file"), settings.value("lastfilepath", QDir::homePath()).toString(), tr("lshw json (*.json)"));
    if (filename.isEmpty()) {
        return;
    }
    qDebug() << "Loading" << filename;
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        statusBar()->showMessage(tr("Failed to open file: %1").arg(file.errorString()));
        return;
    }
    settings.setValue("lastfilepath", QFileInfo(filename).absoluteFilePath());
    parseOutput(file.readAll());
}

void MainWindow::onSave()
{
    if (m_lastRead.isEmpty()) {
        return;
    }
    QSettings settings;
    QString filename = QFileDialog::getSaveFileName(this, tr("Select a file"), settings.value("lastfilepath", QDir::homePath()).toString(), tr("lshw json (*.json)"));
    if (filename.isEmpty()) {
        return;
    }
    qDebug() << "Saving to" << filename;
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        statusBar()->showMessage(tr("Failed to open file for writing: %1").arg(file.errorString()));
        return;
    }
    settings.setValue("lastfilepath", QFileInfo(filename).absoluteFilePath());
    file.write(m_lastRead);
}

void MainWindow::onLshwCompleted(int exitCode, QProcess::ExitStatus exitStatus)
{
    setEnabled(true);
    statusBar()->clearMessage();
    if (exitStatus != QProcess::NormalExit || exitCode != 0) {
        statusBar()->showMessage(tr("Failed to start lshw"));
        return;
    }
    parseOutput(m_lshwProcess->readAllStandardOutput());
    QString errorOutput = QString::fromLocal8Bit(m_lshwProcess->readAllStandardError().trimmed());
    if (!errorOutput.isEmpty()) {
        errorOutput = errorOutput.split('\n').last().simplified();
        QString elided = fontMetrics().elidedText(errorOutput, Qt::ElideRight, statusBar()->width());
        statusBar()->showMessage(elided);
    }
    m_lshwProcess->deleteLater();
}

void MainWindow::launchLshw()
{
    setEnabled(false);
    m_model->clear();
    m_details->setHtml("");
    m_saveAction->setEnabled(false);

    statusBar()->showMessage(tr("Loading..."));
    if (m_lshwProcess) {
        m_lshwProcess->kill();
        m_lshwProcess->deleteLater();
    }
    m_lshwProcess = new QProcess(this);
    connect(m_lshwProcess, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this, &MainWindow::onLshwCompleted);
    connect(m_lshwProcess, &QProcess::errorOccurred, this, [this](QProcess::ProcessError err) {
        statusBar()->showMessage(tr("Failed to launch lshw: %1").arg(m_lshwProcess->errorString()));
        qDebug() << "Error" << err;
    });
    m_lshwProcess->start("lshw", {"-json"});
}

QString MainWindow::createDetailHtml(const QJsonObject &object)
{
    QString ret;

    return ret;
}

static QString sizeAttributeHtml(const QString &name, const QJsonObject &object)
{
    if (!object.contains(name)) {
        return QString();
    }
    if (!object[name].isDouble()) {
        return QString();
    }
    const qint64 size = object[name].toDouble();
    return "<div><b>" + name + "</b>:" + QLocale::system().formattedDataSize(size) + "<br/></div>\n";
}

static QString jsonToHtml(const QString &name, const QJsonObject &object)
{
    QString value;
    if (object[name].isString()) {
        value = object[name].toString();
    } else if (object[name].isDouble()) {
        int intVal = object[name].toInt(-1337);
        if (intVal == -1337) { // idk
            value = QString::number(object[name].toDouble());
        } else {
            value = QString::number(intVal);
        }
    } else if (object[name].isBool()) {
        value = object[name].toBool() ? QObject::tr("Yes") : QObject::tr("No");
    } else if (object[name].isArray()) {
        QStringList vals;
        for (const QJsonValue &val : object[name].toArray()) {
            vals.append(val.toString());
        }
        value = vals.join(", ");
    } else {
        qWarning() << "Weird json value" << object[name];
        return QString();
    }
    if (value.isEmpty()) {
        qWarning() << "Missing value for" << name;
    }
    return value;
}

static QString attributeHtml(const QString &name, const QJsonObject &object, const QString &formatString = QString())
{
    if (!object.contains(name)) {
        return "";
    }
    QString value = jsonToHtml(name, object);
    if (value.isEmpty()) {
        return value;
    }
    if (formatString.isEmpty()) {
        return "<div><b>" + name + ":</b> " + value.toHtmlEscaped() + "<br/></div>";
    } else {
        return "<div><b>" + name + ":</b> " + formatString.arg(value.toHtmlEscaped()) + "<br/></div>";
    }
}

static QString attributeListHtml(const QString &name, const QJsonObject &object)
{
    if (!object.contains(name)) {
        return QString();
    }
    const QJsonObject child = object[name].toObject();
    if (child.isEmpty()) {
        return QString();
    }
    if (child.keys().isEmpty()) {
        qWarning() << "Missing child";

    }
    QString html = "<div>";
    html += "<h3>" + name.toHtmlEscaped() + "</h3>\n";
    html += "<ul style='list-style-type: none'>\n";
    for (const QString &key : child.keys()) {
        QJsonValue val = child.value(key);
        html += "<li><i>" + key;
        if (val.isString()) {
            html +=  ": " + child[key].toString().toHtmlEscaped();
        } else if (!val.isBool() || !val.toBool()) {
            QString htmlVal = jsonToHtml(key, child);
            if (!htmlVal.isEmpty()) {
                html += ": " + htmlVal;
            }
        }
        html += "</i></li>\n";
    }
    html += "</ul>\n";
    return html + "</div>";
}

static QString formatDecimalKilos(unsigned long long value)
{
  const char *prefixes = "KMGTPEZY";
  unsigned int i = 0;

  while ((i <= strlen(prefixes)) && ((value > 10000) || (value % 1000 == 0))) {
    value = value / 1000;
    i++;
  }

  if ((i > 0) && (i <= strlen(prefixes))) {
      return QString::number(value) + prefixes[i - 1];
  }
  return QString::number(value);
}

QStandardItem *MainWindow::createItem(const QJsonObject &object, int depth)
{
    const QString description = object["description"].toString();
    const QString product = object["product"].toString();
    const QString id = object["id"].toString();
    const QString itemClass = object["class"].toString();
    const bool disabled = object["disabled"].toBool();
    const bool claimed = object["claimed"].toBool();
    const QString icon = object["icon"].toString();
    const QString busIcon = object["bus.icon"].toString();
    const QString logo = object["logo"].toString();
    
    QString title;
    if (!description.isEmpty()) {
        title = description;
    } else if (!product.isEmpty()) {
        title = product;
    } else if (!id.isEmpty()) {
        title = id;
    } else {
        qWarning() << "Invalid object" << object.keys();
        title = tr("Unknown");
    }

    QString html;
    html += "<h1>" + title.toHtmlEscaped() + "</h1>";

    if (!claimed || disabled) {
        html += "<span color=\"gray\">";
        if (disabled) {
            html += tr("DISABLED");
        }
        if (!claimed) {
            html += tr("UNCLAIMED");
        }
        html += "</span>\n";
    }

    html += "<br/>\n";

    if (!icon.isEmpty()) {
        qDebug() << "Got icon" << icon;
        html += "<img src='icon:" + icon.toHtmlEscaped() + "' />\n";
    }
    if (!busIcon.isEmpty()) {
        html += "<img src='icon:" + busIcon.toHtmlEscaped() + "' />\n";
    }
    if (!logo.isEmpty()) {
        html += "<img src='icon:" + logo.toHtmlEscaped() + "' />\n";
    }

    html += "<br/>\n";
    html += "<br/>\n";

    static const QStringList standardAttributes = {
        "product", "vendor", "bus info", "logicalname",
        "version", "serial", "slot"
    };

    for (const QString &attribute : standardAttributes) {
        html += attributeHtml(attribute, object);
    }
    html += sizeAttributeHtml("size", object);
    html += sizeAttributeHtml("capacity", object);
    html += attributeHtml("width", object, tr("%1 bits"));

    if (object.contains("clock") && object["clock"].isDouble()) {
        const double clock = object["clock"].toDouble();
        html += "<b>clock</b>: " + formatDecimalKilos(clock);
        if (itemClass == "memory" && clock > 0) {
            html += " (" + tr("%1 ns", "nanosecond clock speed").arg(qint64(1.0e9 / clock)) + ")";
        }
        html += "<br/>\n";
    }

    html += attributeListHtml("capabilities", object);
    html += attributeListHtml("configuration", object);
    html += attributeListHtml("resources", object);

    QStandardItem  *item = new QStandardItem(title);
    item->setData(html, Qt::UserRole);
    item->setEditable(false);

    QJsonArray children = object["children"].toArray();
    for (int i=0; i<children.count(); i++) {
        item->appendRow(createItem(children[i].toObject(), depth + 1));
    }
    return item;
}

void MainWindow::parseOutput(const QByteArray &data)
{
    m_details->clear();
    m_model->clear();
    m_saveAction->setEnabled(false);

    QJsonParseError err;
    const QJsonDocument doc = QJsonDocument::fromJson(data, &err);
    if (err.error != QJsonParseError::NoError) {
        qWarning() << err.errorString() << err.offset;
        statusBar()->showMessage(tr("Invalid JSON"));
        return;
    }

    QJsonObject root;
    if (doc.isObject()) {
        root = doc.object();
    } else if (doc.isArray()) {
        QJsonArray arr = doc.array();
        if (arr.count() != 1) {
            statusBar()->showMessage(tr("Failed to parse data, no array"));
            return;
        }
        root = arr.at(0).toObject();
    } else {
        statusBar()->showMessage(tr("Invalid json"));
        return;
    }
    if (root.isEmpty()) {
        statusBar()->showMessage(tr("Failed to get data"));
        return;
    }

    // Re-add
    for (const QString &url : m_icons.keys()) {
        m_details->document()->addResource(QTextDocument::ImageResource, url, m_icons[url]);
    }
    QStandardItem *rootItem = createItem(root, 0);
    m_model->appendRow(rootItem);
    m_columns->selectionModel()->setCurrentIndex(rootItem->index(), QItemSelectionModel::Select);
    //emit m_columns->clicked(rootItem->index());

    m_lastRead = data;
    m_saveAction->setEnabled(true);
    m_columns->setFocus();
}
