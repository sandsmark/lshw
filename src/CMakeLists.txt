add_subdirectory(po)
add_subdirectory(gui)

include(GNUInstallDirs)
configure_file(core/config.h.in ${CMAKE_CURRENT_BINARY_DIR}/config.h)

set(sources
    lshw.cc
    core/abi.cc
    core/blockio.cc
    core/burner.cc
    core/cdrom.cc
    core/cpufreq.cc
    core/cpuid.cc
    core/cpuinfo.cc
    core/device-tree.cc
    core/disk.cc
    core/display.cc
    core/dmi.cc
    core/dump.cc
    core/fat.cc
    core/fb.cc
    core/graphics.cc
    core/heuristics.cc
    core/hw.cc
    core/ide.cc
    core/ideraid.cc
    core/input.cc
    core/isapnp.cc
    core/jedec.cc
    core/lvm.cc
    core/main.cc
    core/mem.cc
    core/mmc.cc
    core/mounts.cc
    core/network.cc
    core/nvme.cc
    core/options.cc
    core/osutils.cc
    core/parisc.cc
    core/partitions.cc
    core/pci.cc
    core/pcmcia.cc
    core/pcmcia-legacy.cc
    core/pnp.cc
    core/print.cc
    core/s390.cc
    core/scsi.cc
    core/smp.cc
    core/sound.cc
    core/spd.cc
    core/sysfs.cc
    core/usb.cc
    core/version.cc
    core/vio.cc
    core/virtio.cc
    core/volumes.cc
)

if (SQLITE AND sqlite_FOUND)
    set(sources core/db.cc ${sources})
endif()

add_executable(lshw ${sources})

target_include_directories(lshw PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/core ${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(lshw resolv)

if (UDEV)
    target_link_libraries(lshw udev)
endif()

if (SQLITE)
    target_link_libraries(lshw PkgConfig::sqlite)
endif()

if(ZLIB)
    target_link_libraries(lshw ZLIB::ZLIB)
endif()

install(
    TARGETS
        lshw
    PERMISSIONS
        OWNER_READ OWNER_WRITE OWNER_EXECUTE
        GROUP_READ GROUP_EXECUTE
        WORLD_READ WORLD_EXECUTE
        SETUID
    )
install(FILES lshw.1 DESTINATION ${CMAKE_INSTALL_MANDIR})
install(
    FILES pci.ids usb.ids oui.txt manuf.txt pnp.ids pnpid.txt
    DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}/lshw"
)
