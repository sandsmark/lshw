#include "version.h"
#include "hw.h"
#include "sysfs.h"
#include "osutils.h"
#include "input.h"
#include "disk.h"
#include "heuristics.h"
#include "config.h"

#ifdef __GNUC__
#define POPCNT(x) __builtin_popcountll(x)
#else
static inline uint64_t our_popcount64(uint64_t x)
{
  uint64_t m1 = 0x5555555555555555ll;
  uint64_t m2 = 0x3333333333333333ll;
  uint64_t m4 = 0x0F0F0F0F0F0F0F0Fll;
  uint64_t h01 = 0x0101010101010101ll;

  x -= (x >> 1) & m1;
  x = (x & m2) + ((x >> 2) & m2);
  x = (x + (x >> 4)) & m4;

  return (x * h01) >> 56;
}
#define POPCNT(x) our_popcount64(x)
#endif

#include <vector>
#include <iostream>
#include <cstring>

__ID("@(#) $Id$");

#define BUS_PCI                 0x01
#define BUS_ISAPNP              0x02
#define BUS_USB                 0x03
#define BUS_HIL                 0x04
#define BUS_BLUETOOTH           0x05
#define BUS_VIRTUAL             0x06

#define BUS_ISA                 0x10
#define BUS_I8042               0x11
#define BUS_XTKBD               0x12
#define BUS_RS232               0x13
#define BUS_GAMEPORT            0x14
#define BUS_PARPORT             0x15
#define BUS_AMIGA               0x16
#define BUS_ADB                 0x17
#define BUS_I2C                 0x18
#define BUS_HOST                0x19
#define BUS_GSC                 0x1A
#define BUS_ATARI               0x1B
#define BUS_SPI                 0x1C
#define BUS_RMI                 0x1D
#define BUS_CEC                 0x1E
#define BUS_INTEL_ISHTP         0x1F

#ifdef UDEV
#include <libudev.h>

std::string iconFromUdev(udev_device *dev)
{
    if (!dev) {
        return "";
    }

    // Available IDs:
    // ID_INPUT, ID_INPUT_ACCELEROMETER, ID_INPUT_MOUSE, ID_INPUT_POINTINGSTICK,
    // ID_INPUT_TOUCHSCREEN, ID_INPUT_TOUCHPAD, ID_INPUT_TABLET,
    // ID_INPUT_TABLET_PAD, ID_INPUT_JOYSTICK, ID_INPUT_KEY, ID_INPUT_KEYBOARD,
    // ID_INPUT_SWITCH, ID_INPUT_TRACKBALL

    // Available icons:
    // input-gamepad
    // input-keyboard
    // input-mouse
    // input-tablet
    // input-touchpad
    // speaker
    // audio-card
    // audio-input-microphone

    if (udev_device_get_property_value(dev, "ID_INPUT_TOUCHPAD")) {
        return "input-touchpad";
    }
    if (udev_device_get_property_value(dev, "ID_INPUT_POINTINGSTICK")) {
        return "trackpoint";
    }
    if (udev_device_get_property_value(dev, "ID_INPUT_TABLET")) {
        return "input-tablet";
    }
    if (udev_device_get_property_value(dev, "ID_INPUT_MOUSE")) {
        return "input-mouse";
    }
    if (udev_device_get_property_value(dev, "ID_INPUT_KEYBOARD")) {
        return "input-keyboard";
    }
    const char *type = udev_device_get_property_value(dev, "ID_TYPE");
    if (type) {
        if (strcmp(type, "video") == 0) {
            return "camera-web";
        }
    }

    return "";
}

#endif

using namespace std;

bool scan_input(hwNode & n)
{
  vector < sysfs::entry > entries = sysfs::entries_by_class("input");

  if (entries.empty())
    return false;

#ifdef UDEV
  udev *udevContext = udev_new();
#endif

  for (vector < sysfs::entry >::iterator it = entries.begin();
      it != entries.end(); ++it)
  {
    const sysfs::entry & e = *it;

    if(!e.hassubdir("id")) continue;

    hwNode *device = n.findChildByBusInfo(e.leaf().businfo());
    if(!device)
    {
      device = n.findChildByBusInfo(e.leaf().parent().businfo());
      if(device)
        device = device->addChild(hwNode("input", hw::input));
    }
    if(!device)
      device = n.addChild(hwNode("input", hw::input));
    else
    {
      if(device->getClass() == hw::generic)
        device->setClass(hw::input);
    }
    device->claim();
    device->setLogicalName("input/"+e.name());
    if (device->getProduct().empty())
      device->setProduct(e.string_attr("name"));
    device->setModalias(e.modalias());

    std::string icon = "";

#ifdef UDEV
    if (udevContext) {
      udev_device *dev = udev_device_new_from_subsystem_sysname(udevContext, "input", e.name().c_str());
      if (dev) {
        icon = iconFromUdev(dev);
        udev_device_unref(dev);
      }
    }
#endif

    if (icon.empty()) {
      std::string keys = e.string_attr("capabilities/key");
      if (!keys.empty()) {
        if (keys.back() == '\n') {
            keys.pop_back();
        }

        const size_t lastSpace = keys.find_last_of(' ');
        if (lastSpace != std::string::npos) {
          keys = keys.substr(lastSpace + 1);
        }
        const unsigned long long keymask = strtoull(keys.c_str(), NULL, 16);
        if (keymask && POPCNT(keymask) > 50) {
          icon = "input-keyboard";
        }
      }
    }

    if (!icon.empty()) {
      device->addHint("icon", icon);
    }


    switch(e.hex_attr("id/bustype"))
    {
      case BUS_PCI:
	      device->addCapability("pci", "PCI");
	      break;
      case BUS_ISAPNP:
	      device->addCapability("isa", "ISA");
	      device->addCapability("pnp", "PnP");
	      break;
      case BUS_USB:
	      device->addCapability("usb", "USB");
	      break;
      case BUS_HIL:
	      device->addCapability("hil", "HP-HIL");
	      break;
      case BUS_BLUETOOTH:
	      device->addCapability("bluetooth", "Bluetooth");
	      break;
      case BUS_VIRTUAL:
	      device->addCapability("virtual");
	      break;
      case BUS_ISA:
	      device->addCapability("isa", "ISA bus");
	      break;
      case BUS_I8042:
	      device->addCapability("i8042", "i8042 PC AT keyboard controller");
	      break;
      case BUS_XTKBD:
	      device->addCapability("xtkbd", "XT keyboard controller");
	      break;
      case BUS_RS232:
	      device->addCapability("rs232", "RS-232 serial");
	      break;
      case BUS_GAMEPORT:
	      device->addCapability("gameport", "game port");
	      break;
      case BUS_PARPORT:
	      device->addCapability("parport", "parallel port");
	      break;
      case BUS_AMIGA:
	      device->addCapability("amiga", "Amiga bus");
	      break;
      case BUS_ADB:
	      device->addCapability("adb", "Apple Desktop Bus");
	      break;
      case BUS_I2C:
	      device->addCapability("i2c", "I²C bus");
	      break;
      case BUS_HOST:
	      device->addCapability("platform");
	      break;
      case BUS_GSC:
	      device->addCapability("gsc", "GSC bus");
	      break;
      case BUS_ATARI:
	      device->addCapability("atari", "Atari bus");
	      break;
      case BUS_SPI:
	      device->addCapability("spi", "SPI");
	      break;
      case BUS_RMI:
	      device->addCapability("rmi", "RMI");
	      break;
      case BUS_CEC:
	      device->addCapability("cec", "CEC");
	      break;
      case BUS_INTEL_ISHTP:
	      device->addCapability("intelishtp", "Intel Integrated Sensor Hub");
	      break;
    }

    vector < sysfs::entry > events = e.devices();
    for(vector < sysfs::entry >::iterator i = events.begin(); i != events.end(); ++i)
    {
      device->setLogicalName("input/"+i->name());
    }
  }

#ifdef UDEV
  if (udevContext) {
    udev_unref(udevContext);
  }
#endif

  return true;
}
